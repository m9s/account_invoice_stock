#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:account.invoice.line,stock_moves:"
msgid "Stock Moves"
msgstr ""

#, fuzzy
msgctxt "field:account.invoice.line-stock.move,create_date:"
msgid "Create Date"
msgstr "Datum"

#, fuzzy
msgctxt "field:account.invoice.line-stock.move,create_uid:"
msgid "Create User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:account.invoice.line-stock.move,id:"
msgid "ID"
msgstr "ID"

#, fuzzy
msgctxt "field:account.invoice.line-stock.move,invoice_line:"
msgid "Invoice Line"
msgstr "Factuurregel"

msgctxt "field:account.invoice.line-stock.move,rec_name:"
msgid "Record Name"
msgstr ""

msgctxt "field:account.invoice.line-stock.move,stock_move:"
msgid "Stock Move"
msgstr ""

#, fuzzy
msgctxt "field:account.invoice.line-stock.move,write_date:"
msgid "Write Date"
msgstr "Schrijfdatum"

#, fuzzy
msgctxt "field:account.invoice.line-stock.move,write_uid:"
msgid "Write User"
msgstr "Gebruiker"

#, fuzzy
msgctxt "field:stock.move,invoice_lines:"
msgid "Invoice Lines"
msgstr "Factuurregels"

msgctxt "model:account.invoice.line-stock.move,name:"
msgid "Invoice Line - Stock Move"
msgstr ""

#, fuzzy
msgctxt "view:account.invoice.line:"
msgid "Stock"
msgstr "Voorraad"
